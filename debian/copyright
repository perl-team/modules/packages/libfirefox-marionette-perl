Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Firefox-Marionette
Upstream-Contact: David Dick <ddick@cpan.org>
Source: https://metacpan.org/release/Firefox-Marionette
Comment:
 lib/Firefox/Marionette/Extension/HarExportTrigger.pm contains a
 base64-encoded zip archive which is, when decoded, byte-identical to the
 Firefox addon har_export_trigger-0.6.1-an+fx.xpi from
 https://addons.mozilla.org/en-US/firefox/addon/har-export-trigger/ .
 .
 This xpi file is identical (modulo release artifacts) to the source code of
 the har-export-trigger 0.6.1 release at
 https://github.com/firefox-devtools/har-export-trigger/releases .
 .
 https://codeload.github.com/firefox-devtools/har-export-trigger/zip/refs/tags/0.6.1
 has been downloaded as har-export-trigger-0.6.1.zip and unpacked into
 debian/missing-sources.

Files: *
Copyright: 2024, David Dick <ddick@cpan.org>
License: Artistic or GPL-1+

Files: lib/Firefox/Marionette/Extension/HarExportTrigger.pm
Copyright: 2024, David Dick <ddick@cpan.org>
License: Artistic or GPL-1+, and MPL-2.0
Comment:
 From the module documentation:
 .
 This module also contains software written by Jan Odvarko
 <http://www.softwareishard.com/blog/about/> that is licensed under the
 Mozilla Public License 2.0 <https://www.mozilla.org/en-US/MPL/2.0/>.
 .
 From the general documentation:
 .
 The Firefox::Marionette::Extension::HarExportTrigger module includes the HAR
 Export Trigger <https://github.com/firefox-devtools/har-export-trigger>
 extension which is licensed under the Mozilla Public License 2.0
 https://www.mozilla.org/en-US/MPL/2.0/.

Files: t/addons/borderify/*
Copyright: Mozilla
License: MPL-2.0
Comment: Source: https://github.com/mdn/webextensions-examples/tree/main/borderify

Files: debian/*
Copyright: 2021-2024, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

Files: debian/missing-sources/har-export-trigger-0.6.1/*
Copyright: Jan Odvarko
License: MPL-2.0
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: MPL-2.0
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0.
 On Debian systems, the complete text of Mozilla Public License v 2.0
 can be found in '/usr/share/common-licenses/MPL-2.0'.
